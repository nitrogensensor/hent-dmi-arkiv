package dk.dtu.nitrogen.hent_fra_dmi_vejrarkiv;
import org.json.JSONArray;
import org.json.JSONObject;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.net.URLEncoder;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.time.LocalDate;

import java.util.*;

/**
 * Henter vejrdata fra DMI - se https://www.dmi.dk/vejrarkiv/
 */
@SuppressWarnings("NonAsciiCharacters")
public class HentFraDmi {

	public static void main(String[] args) {
		try {
			//Locale.setDefault(new Locale("da", "DK"));

			//String[] monednavne = new String[] { "Januar", "Februar", "Marts", "April", "Maj", "Juni", "Juli", "August", "September", "Oktober", "November", "December"};

			String[] bynavne = new String[] {
					"Albertslund","Allerød","Assens","Ballerup","Billund","Bornholm","Brøndby","Brønderslev","Dragør",
					"Egedal","Esbjerg","Fanø","Favrskov","Faxe","Fredensborg","Fredericia","Frederiksberg","Frederikshavn",
					"Frederikssund","Furesø","Faaborg-Midtfyn","Gentofte","Gladsaxe","Glostrup","Greve","Gribskov","Guldborgsund",
					"Haderslev","Halsnæs","Hedensted","Helsingør","Herlev","Herning","Hillerød","Hjørring","Holbæk","Holstebro",
					"Horsens","Hvidovre","Høje-Taastrup","Hørsholm","Ikast-Brande","Ishøj","Jammerbugt","Kalundborg","Kerteminde",
					"Kolding","København","Køge","Langeland","Lejre","Lemvig","Lolland","Lyngby-Taarbæk","Læsø","Mariagerfjord",
					"Middelfart","Morsø","Norddjurs","Nordfyns","Næstved","Nyborg","Odder","Odense","Odsherred","Randers","Rebild",
					"Ringkøbing-Skjern","Ringsted","Roskilde","Rudersdal","Rødovre","Samsø","Silkeborg","Skanderborg","Skive",
					"Slagelse","Solrød","Sorø","Stevns","Struer","Svendborg","Syddjurs","Sønderborg","Thisted","Tønder","Tårnby",
					"Vallensbæk","Varde","Vejen","Vejle","Vesthimmerlands","Viborg","Vordingborg","Ærø","Aabenraa","Aalborg",
					"Aarhus" };

			// Muligheder: temperature, humidity, precip, pressure, sunhours, lightning, wind, winddir
			String[] datatyper  = new String[] {
					"temperature", "humidity", "precip", "pressure", "sunhours", "lightning", "winddir", "wind"
			};

			// Ændre start- og slutdato til det ønskede, samt bynavn én eller flere bynavne
			Date startdato = java.sql.Date.valueOf(LocalDate.parse("2019-12-01"));
			Date slutdato = java.sql.Date.valueOf(LocalDate.parse("2019-12-12"));
			// Kan tilføjes flere byer
			String[] valgteByer = new String[] {
					"Ballerup"//, "Høje-Taastrup"
			};

			for (String valgtBy : valgteByer) {

				for (String datatype : datatyper) {
					opretCsv(datatype, valgtBy, startdato, slutdato);
				}
			}

		} catch (Exception e) {
			System.out.flush();
			e.printStackTrace();
		}
	}




	public static List<Date> generérDatoerIInterval(Date startdato, Date slutdato){
		ArrayList<Date> dates = new ArrayList<>();
		Calendar cal1 = Calendar.getInstance();
		cal1.setTime(startdato);
		Calendar cal2 = Calendar.getInstance();
		cal2.setTime(slutdato);

		while(cal1.before(cal2) || cal1.equals(cal2))
		{
			dates.add(cal1.getTime());
			cal1.add(Calendar.DATE, 1);
		}
		return dates;
	}


	private static void opretCsv(String valgtDatatype, String valgtBy, Date startdato, Date slutdato) throws IOException {
		File resultatmappe = new File("resultat");
		//noinspection ResultOfMethodCallIgnored
		resultatmappe.mkdirs();
		String outputfilnavn = "data_" + valgtBy + "_" + valgtDatatype + ".csv";
		FileWriter outputfil = new FileWriter(new File(resultatmappe, outputfilnavn));
		String grundUrl = "https://www.dmi.dk/dmidk_obsWS/rest/archive/hourly/danmark/" + valgtDatatype + "/" + URLEncoder.encode(valgtBy, "UTF-8") + "/";
		System.out.println("Henter " + valgtDatatype + " fra " + valgtBy + " i intervallet " + generérDatoerIInterval(startdato, slutdato));

		List<String> fasteKolonnenavne = Arrays.asList("time", "dateLocalString", "dateString");

		// Hent data for en dag, sådan at vi kan se hvad kolonnenavnene hedder for denne type data
		String data0 = hentUrl(grundUrl + "2019/Juli/03");
		LinkedHashMap<String, String> dataKolonnenavneOgTitler = findKolonnenavneOgTitler(lavJSONArray(data0));
		System.out.println("dataKolonnenavneOgTitler: " + dataKolonnenavneOgTitler);


		// Lav første række med kolonnenavne
		StringBuilder outputKolonnenavne = new StringBuilder();
		for (String kn : fasteKolonnenavne) outputKolonnenavne.append(kn).append(", ");
		for (String kn : dataKolonnenavneOgTitler.values()) outputKolonnenavne.append(kn).append(", ");
		outputKolonnenavne.setLength(outputKolonnenavne.length() - 2); // fjern det sidste ", "
		outputKolonnenavne.append("\n");
		System.out.println(outputKolonnenavne);
		outputfil.append(outputKolonnenavne);


		ArrayList<String> dataKolonnenavne = new ArrayList<>(dataKolonnenavneOgTitler.keySet());
		// Hent for et datointerval
		for(Date dates : generérDatoerIInterval(startdato,slutdato)) {
			SimpleDateFormat format = new SimpleDateFormat("yyyy/MMMM/dd");
			String dato = format.format(dates.getTime());
			String url = grundUrl + dato.substring(0,5)+dato.substring(5,6).toUpperCase()+ dato.substring(6);
			try {
				String data = hentUrl(url);
				JSONArray jsonArray = lavJSONArray(data);
				StringBuilder outputData = lavIndholdAfCsvfilForEnDag(jsonArray, dataKolonnenavne, fasteKolonnenavne);
				//System.out.println(outputData);
				outputfil.append(outputData);

			} catch (Exception e) {
				System.err.println("Fejlet for "+dates + " "+url);
				e.printStackTrace();
			}
		}

		outputfil.close();
	}

	private static JSONArray lavJSONArray(String data) {
		// Nogle svar har form af et JSONArray
		if (data.startsWith("[")) return new JSONArray(data);
		// andre er et enkelt JSON-objekt, der skal pakkes ind
		return new JSONArray().put(new JSONObject(data));
	}

	private static String hentUrl(String url) throws IOException {
		System.out.println("henter "+url);
		FilCache.init(new File("../hent-dmi-arkiv-tmp/"));
		String filnavn = FilCache.hentFil(url, true);
		@SuppressWarnings("UnnecessaryLocalVariable")
		String data = new String(Files.readAllBytes(Paths.get(filnavn)));
		return data;
	}

	/*
time, dateLocalString, dateString, Time middeltemperatur, Time maksimumtemperatur, Time minimumtemperatur,
1562104800000, 03-07-2019 00:00, 02-07-2019 22:00, 12.443928718566895, 13.0, 12.0,
1562108400000, 03-07-2019 01:00, 02-07-2019 23:00, 12.04185676574707, 12.699999809265137, 11.5,
1562112000000, 03-07-2019 02:00, 03-07-2019 00:00, 11.82728099822998, 12.399999618530273, 11.300000190734863,
1562115600000, 03-07-2019 03:00, 03-07-2019 01:00, 11.536797523498535, 12.0, 10.899999618530273,

 */

	private static StringBuilder lavIndholdAfCsvfilForEnDag(JSONArray jsonArray, List<String> dataKolonnenavne, List<String> almKolonnenavne) {

		HashMap<String, JSONArray> dataKolonnenavnTildataserie = new HashMap<>();
		for (int i=0; i<jsonArray.length(); i++) {
			JSONObject obj = jsonArray.getJSONObject(i);
			String parameter = obj.getString("parameter");
			dataKolonnenavnTildataserie.put(parameter, obj.getJSONArray("dataserie"));
		}

		StringBuilder output = new StringBuilder();
		int antalVærdier = jsonArray.getJSONObject(0).getJSONArray("dataserie").length();

		for (int n=0; n<antalVærdier; n++) {
			for (String kolonnenavn : almKolonnenavne) {
				Object værdi = jsonArray.getJSONObject(0).getJSONArray("dataserie").getJSONObject(n).get(kolonnenavn);
				output.append(værdi).append(", ");
			}



			for (String kolonnenavn : dataKolonnenavne) {
				JSONArray dataserie = dataKolonnenavnTildataserie.get(kolonnenavn);

				//System.out.println( n + " "+ kolonnenavn + " "+ dataserie.length());
				Object værdi = dataserie.getJSONObject(n).get("value");
				output.append(værdi).append(", ");
			}
			output.setLength(output.length()-2); // fjern det sidste ", "
			output.append("\n");
		}
		return output;
	}

	private static LinkedHashMap<String,String> findKolonnenavneOgTitler(JSONArray jsonArray) {
		LinkedHashMap<String,String> dataKolonnenavne = new LinkedHashMap<>();
		for (int i=0; i<jsonArray.length(); i++) {
			JSONObject obj = jsonArray.getJSONObject(i);
			String parameter = obj.getString("parameter");
			String enhed = obj.getString("unit");
			String kolonnenavn = parameter;
			if (!enhed.isEmpty()) kolonnenavn += " (" + enhed + ")";
			dataKolonnenavne.put(parameter, kolonnenavn);
		}
		return dataKolonnenavne;
	}
}
