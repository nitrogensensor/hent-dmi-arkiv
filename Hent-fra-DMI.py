import locale
from urllib.request import urlopen
import pandas as pd
import json
import os as os

print("EKSEMPEL på  et foreløbigt Pythin-script. Brug Java-projektet, det er mere komplet")

# For at få en anden parameter end temperatur ændres det i den nedenstående URL:
# Muligheder: temperature, humidity, precip, pressure, sunhours, lightning, wind, winddir
# For område: Ballerup: Ballerup, Høje-Taastrup: H%C3%B8je-Taastrup, København: K%C3%B8benhavns og mange flere endnu som kan findes på dmi.dk/vejrarkiv/
url = "https://www.dmi.dk/dmidk_obsWS/rest/archive/hourly/danmark/temperature/Ballerup/"

# Sæt start- og slutdato
start_date = "2019-10-10"
end_date = "2019-10-15"

month_list = pd.date_range(start=start_date, end=end_date, freq='D')

locale.setlocale(locale.LC_ALL, 'da_DK.UTF-8')

# Windows: sæt C:\Users\Username\ foran dmi i "" for at gemme i en destination
outputFileName = "dmi_#.txt" # kan også sættes til .json filformat
outputFileName = outputFileName.replace("#", start_date+"-"+end_date)

if os.path.exists(outputFileName):
	os.remove(outputFileName)

for i in month_list:
    tempurl = url + i.strftime("%Y") + "/" + i.strftime("%B").capitalize() + "/" + i.strftime("%d")
    print("URL",tempurl)
    f = urlopen(tempurl)
    urls = f.read()
    data = json.loads(urls)
    with open(outputFileName, "a") as dmiDataFile:
        jsonFormatted = json.dumps(data, indent=4, sort_keys=False)  # sort_keys ændrer output udfra keys
        print("sssss:", jsonFormatted)
        dmiDataFile.write(jsonFormatted)
        dmiDataFile.close()
